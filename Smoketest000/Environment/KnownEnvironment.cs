﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoketest000.Env
{
    class KnownEnvironment
    {
        public string EnvironmentName { get; private set; }

        public static readonly KnownEnvironment Dev = new KnownEnvironment("Mattersphere Development (NEW)");
        public static readonly KnownEnvironment Test = new KnownEnvironment("Mattersphere Test");
        public static readonly KnownEnvironment Test5 = new KnownEnvironment("Mattersphere Test 5");
        public static readonly KnownEnvironment UAT = new KnownEnvironment("Mattersphere UAT (NEW)");
        public static readonly KnownEnvironment PreProd = new KnownEnvironment("PRE-PROD");
        public static readonly KnownEnvironment TRAIN = new KnownEnvironment("Mattersphere TRAIN (TRAINDB01)");

        private KnownEnvironment(string name)
        {
            EnvironmentName = name;
        }
        public static string GetEnvironment()
        {
            //Gets selected env from teamcity
            var environmentValue = Environment.GetEnvironmentVariable("MSEnvironment");

            if (string.IsNullOrEmpty(environmentValue))
            {
                // Change env manually when running from Visual Studio

                var env = Env.KnownEnvironment.TRAIN;

                environmentValue = env.EnvironmentName;
            }

            return environmentValue;
        }
    }
}
