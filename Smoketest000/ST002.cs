﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace Smoketest000
{
    /// <summary>
    /// Summary description for Select any document from precedent manager and generate it in word
    /// </summary>
    [CodedUITest]
    public class ST002
    {
        public ST002()
        {
        }
        public string GetEnvironment()
        {
            //Gets selected env from teamcity
            var environmentValue = Environment.GetEnvironmentVariable("MSEnvironment");

            {
                //select environment in the KnownEnvironment.cs file
                var env = Env.KnownEnvironment.GetEnvironment();

            }

            return environmentValue;
        }

        [TestMethod]
        public void ST_002()
        {

            Playback.PlaybackSettings.LoggerOverrideState = HtmlLoggerState.AllActionSnapshot;

            ///open word & connect to environment***********
            //select environment in the KnownEnvironment.cs file
            var envName = GetEnvironment();
            this.UIMap.connecttomattersphere(envName);

            /////open mattersphere ribbon * **************
            this.UIMap.openmattersphereribbon();

            /// test cases * ********
            /// Select precendent document PI-HUM-019 from precedent manager and generate it in word
            this.UIMap.openprecedentinword();


            //close word********************
            this.UIMap.closeword();


        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
