﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace Smoketest000
{

    /// <summary>
    /// Summary description for Mattersphere ribbon
    /// </summary>
    [CodedUITest]
    public class ST000
    {
        public ST000()
        {
        }

        public string GetEnvironment()
        {
            //Gets selected env from teamcity
            var environmentValue = Environment.GetEnvironmentVariable("MSEnvironment");
          
            {
               
                var env = Env.KnownEnvironment.GetEnvironment();
                
            }

            return environmentValue;
        }


        [TestMethod]
        public void ST_000()
        {
            Playback.PlaybackSettings.LoggerOverrideState = HtmlLoggerState.AllActionSnapshot;

            ///open word & connect to environment***********

            //select environment in the KnownEnvironment.cs file
            var envName = GetEnvironment();
            this.UIMap.connecttomattersphere(envName);

            ///open mattersphere ribbon * **************
            this.UIMap.openmattersphereribbon();

            /// test cases * ********
            //open all menu items on mattersphere ribbon * *************
            this.UIMap.commandcentre();
            this.UIMap.contactmanager();

            ///bug logged***RUN-1460**************
            this.UIMap.clientinformation();
            this.UIMap.matterinformation();
            this.UIMap.precedentmanager();
            this.UIMap.quickprecedent();
            this.UIMap.joblist();
            this.UIMap.milestoneinformation();
            this.UIMap.about();
            this.UIMap.changeusersetting();

            // bug logged**RUN-1464*****************
            this.UIMap.conflictsearcher();

            //New Entry menu**************
            this.UIMap.newmatter();
            this.UIMap.newcontact();
            this.UIMap.newassociate();
            this.UIMap.newclient();
            this.UIMap.individualnewclient();
            this.UIMap.corporatepreclient();
            this.UIMap.appointment();
            this.UIMap.archive();
            this.UIMap.associate();
            this.UIMap.compliant();
            this.UIMap.documentreciept();
            this.UIMap.keydate();
            this.UIMap.note();
            this.UIMap.riskassessment();
            this.UIMap.task();
            this.UIMap.undertaking();
            this.UIMap.mailmessage();

            //To Associate menu
            this.UIMap.toassociate();
            this.UIMap.toclient();
            this.UIMap.newmemo();
            this.UIMap.newcomplimentslip();
            this.UIMap.pickletterhead();

            //Notes menu
            this.UIMap.notes();
            this.UIMap.meeting();
            this.UIMap.attendancenote();
            this.UIMap.telephonein();
            this.UIMap.telephoneout();

            //Documents menu
            this.UIMap.blankdocument();
            this.UIMap.courtdocument();
            this.UIMap.accountsslip();
            this.UIMap.financialstatement();
            this.UIMap.invoicing();

            //Reports menu
            this.UIMap.corereports();
       
            
            //External Apps menu
            this.UIMap.intranetbrowser();
            this.UIMap.microsoftwebsite();
            this.UIMap.externalapplication1();
            this.UIMap.externalapplication2();

            //Outlook menu

            //close word********************
            this.UIMap.closeword();




        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
