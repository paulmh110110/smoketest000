﻿#Read in Environment Variables for merging into config files
$EnvironmentVariables = get-childitem -path env:* | get-item

$Dir = get-childitem .\ -recurse
$List = $Dir | where {$_.extension -eq ".config"} | where {$_.name -ne "Transforms.config"} | where {$_.name -ne "packages.config"}

if ($List.Length -gt 0) {

	#Load Transforms.config
	[xml]$TransformsConfig = Get-Content -Path .\Transform\Transforms.config

	foreach ($configFile in $List) {
		Echo "Transforming file: $($configFile.FullName)"
        #Load in the config file we want to transform
        [xml]$TransformedFile = Get-Content -Path ($configFile | Resolve-Path -Relative)

        #Load in the config settings section thats applicable for this config file
        $FileSettings = $TransformsConfig.configuration.file | where {$_.value -eq ($configFile | Resolve-Path -Relative)}

		foreach($envVariable in $EnvironmentVariables) {
            #Check if there's a config setting for this Key
            $TransformSetting = $FileSettings.map | where {$_.tcKey -eq $envVariable.Name}

            if ($TransformSetting -eq $null){
                #Do default transform of value attribute
                #First try to find nodes based on key attribute
				$matchedNodes = $TransformedFile.SelectNodes("//*[@key='$($envVariable.Name)']")

                #If no nodes try to find nodes based on name attribute
                if ($matchedNodes.Count -eq 0){
                    $matchedNodes = $TransformedFile.SelectNodes("//*[@name='$($envVariable.Name)']")
                }
            }else{
                #Do transform according to settings
				if ([string]::IsNullOrEmpty($TransformSetting.fileKey) -ne $true){
					#Find nodes based on key attribute
					$matchedNodes = $TransformedFile.SelectNodes("//*[@key='$($TransformSetting.fileKey)']")
				}

				if ([string]::IsNullOrEmpty($TransformSetting.fileName) -ne $true){
					#Find nodes based on name attribute
					$matchedNodes = $TransformedFile.SelectNodes("//*[@name='$($TransformSetting.fileName)']")
				}

				if ([string]::IsNullOrEmpty($TransformSetting.fileElement) -ne $true){
					#Find nodes based on element type
					$matchedNodes = $TransformedFile.SelectNodes("//$($TransformSetting.fileElement)")
				}
            }

            foreach($node in $matchedNodes){
				if ([string]::IsNullOrEmpty($TransformSetting.fileValueAttribute) -ne $true){
					$node.SetAttribute($TransformSetting.fileValueAttribute, $envVariable.Value)
				}else{
                    #Default transform - replace either the value or connectionString attribute
                    if ($node.HasAttribute("value")){
                        $node.SetAttribute("value", $envVariable.Value)
                    }elseif ($node.HasAttribute("connectionString")){
                        $node.SetAttribute("connectionString", $envVariable.Value)
                    }
				}
			}
		}

        $TransformedFile.Save($configFile.FullName)
	}

}